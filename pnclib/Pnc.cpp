#include "Pnc.h"

#define TRIGER_PIN   10
#define ECHO_PIN    11
#define BUTTON    2
#define BUZZER    8
#define SERVOMOTOR 9

#define NOT_SET -1

#define MIN_PIN 2
#define MAX_PIN 13

// Status of a pin (INPUT/OUTPUT)
// -1: not set
// 0 : INPUT
// 1 : OUTPUT
//
int pinStatus[14] = {NOT_SET, NOT_SET, NOT_SET, NOT_SET, NOT_SET, NOT_SET,
                     NOT_SET, NOT_SET, NOT_SET, NOT_SET, NOT_SET, NOT_SET,
                     NOT_SET, NOT_SET};
bool serialStarted = false;
int POTENTIOMETER = A0;
int LDR = A1;
bool isPinDefaultOutput = false;

// the last time the output pin was toggled
unsigned long lastDebounceTime = 0;
// the debounce time; increase if the output flickers
unsigned long debounceDelay = 50;
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

int getMinPin() {
  return MIN_PIN;
}
int getMaxPin() {
  return MAX_PIN;
}


int getLedPin(int ledNumber) {
  if (ledNumber < getMinPin() || ledNumber > getMaxPin()) {
    return -1;
  } else {
    return ledNumber;
  }
}

void defaultInitToOutput() {
  if (!isPinDefaultOutput) {
    for (int i = 2; i <= 13; i++) {
      pinMode(i, OUTPUT);
    }
    isPinDefaultOutput = true;
  }
}

void initSerial() {
  defaultInitToOutput();
  if (!serialStarted) {
    Serial.begin(9600);
    serialStarted = true;
  }
}

bool setPinInput(int pin) {
  defaultInitToOutput();
  if (pinStatus[pin] == NOT_SET) {
    pinStatus[pin] = INPUT;
    pinMode(pin, INPUT);
  }
  return pinStatus[pin] == INPUT;
}
bool setPinInputPullup(int pin) {
  defaultInitToOutput();
  if (pinStatus[pin] == NOT_SET) {
    pinStatus[pin] = INPUT_PULLUP;
    pinMode(pin, INPUT_PULLUP);
  }
  return pinStatus[pin] == INPUT_PULLUP;
}
bool setPinOutput(int pin) {
  defaultInitToOutput();
  if (pinStatus[pin] == NOT_SET) {
    pinStatus[pin] = OUTPUT;
    pinMode(pin, OUTPUT);
  }
  return pinStatus[pin] == OUTPUT;
}

void setLedOn(int ledNumber) {
  int ledPin = getLedPin(ledNumber);
  if (ledPin != -1 && setPinOutput(ledPin)) {
    digitalWrite(ledPin, HIGH);
  }
}
void setLedOff(int ledNumber) {
  int ledPin = getLedPin(ledNumber);
  if (ledPin != -1 && setPinOutput(ledPin)) {
    digitalWrite(ledPin, LOW);
  }
}

void setAllLedOn() {
  for (int i = getMinPin(); i <= getMaxPin(); i ++) {
    setLedOn(i);
  }
}
void setAllLedOff() {
  for (int i = getMinPin(); i <= getMaxPin(); i ++) {
    setLedOff(i);
  }
}
void toggleLed(int ledNumber) {
  int ledPin = getLedPin(ledNumber);
  if (ledPin != -1 && setPinOutput(ledPin)) {
    digitalWrite(ledPin, digitalRead(ledPin) == HIGH ? LOW : HIGH);
  }
}
int getDistanceInCm() {
  if (setPinOutput(TRIGER_PIN) && setPinInput(ECHO_PIN)) {
    // Send a PULSE of 15ms on TRIGGER PIN to initialize a reading
    digitalWrite(TRIGER_PIN, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIGER_PIN, HIGH);
    delayMicroseconds(15);
    digitalWrite(TRIGER_PIN, LOW);

    // Waits for the pin to go HIGH, starts timing,
    // then waits for the pin to go LOW and stops timing.
    // Returns the length of the pulse in microseconds.
    long pulseLenghtMs = pulseIn(ECHO_PIN, HIGH, 1000000);

    // The speed of sound is 340 m/s or 29 microseconds per centimeter.
    // The ping travels out and back, so to find the distance of the
    // object we take half of the distance travelled.
    return round(pulseLenghtMs / 29 / 2);
  } else {
    return 0;
  }
}

void print(char* text) {
  initSerial();
  Serial.print(text);
}
void print(int value) {
  initSerial();
  Serial.print(value);
}
void print(boolean value) {
  initSerial();
  if (value) {
    print(static_cast<char *>("true"));
  } else {
    print(static_cast<char *>("false"));
  }
}
void print(String value) {
  initSerial();
  Serial.print(value);
}
void println(char* text) {
  initSerial();
  Serial.println(text);
}
void println(int value) {
  initSerial();
  Serial.println(value);
}
void println(boolean value) {
  initSerial();
  if (value) {
    println(static_cast<char *>("true"));
  } else {
    println(static_cast<char *>("false"));
  }
}
void println(String value) {
  initSerial();
  Serial.println(value);
}
bool isDataAvailable() {
  initSerial();
  return Serial.available() > 0;
}
int readData() {
  initSerial();
  return Serial.read();
}
char readDataChar() {
  initSerial();
  return static_cast<char>(readData());
}
String readDataLine() {
  initSerial();
  String line = "";
  while (isDataAvailable()) {
    char c = readDataChar();
    if (c != '\n') {
      line += c;
    }
    delay(10);
  }
  return line;
}

bool isButtonPressed() {
  if (setPinInputPullup(BUTTON)) {
    return !digitalRead(BUTTON);
  } else {
    return false;
  }
}
bool debounceClick() {
  int firstValue;
  int secondValue;
  int thirdValue;
  do {
    firstValue = isButtonPressed();
    delay(1);
    secondValue = isButtonPressed();
    delay(1);
    thirdValue = isButtonPressed();
  } while (firstValue != secondValue || firstValue != thirdValue);
  return firstValue;
}

bool haveClicked() {
  if (debounceClick()) {
    while (debounceClick()) {
      /* code */
    }
    return true;
  }
  return false;
}

void playSound(int frequency) {
  if (setPinOutput(BUZZER)) {
    tone(BUZZER, frequency);
  }
}
void playSound(int frequency, int duration) {
  if (setPinOutput(BUZZER)) {
    tone(BUZZER, frequency, duration);
  }
}
void stopSound() {
  if (setPinOutput(BUZZER)) {
    noTone(BUZZER);
  }
}
void successMusic() {
  for (int i = 0; i < 2; i++) {
    playSound(880);
    delay(100);
    playSound(988);
    delay(100);
    playSound(523);
    delay(100);
    playSound(988);
    delay(100);
    playSound(523);
    delay(100);
    playSound(587);
    delay(100);
    playSound(523);
    delay(100);
    playSound(587);
    delay(100);
    playSound(659);
    delay(100);
    playSound(587);
    delay(100);
    playSound(659);
    delay(100);
    playSound(659);
    delay(100);
  }
  stopSound();
}
void failMusic() {
  playSound(392);
  delay(250);
  playSound(262);
  delay(500);
  stopSound();
}

int getMaxPotentiometerValue() {
  return 1023;
}
int getPotentiometerValue(int maxValue) {
  defaultInitToOutput();
  return static_cast<int>(static_cast<float>(analogRead(POTENTIOMETER)) /
    getMaxPotentiometerValue() * (maxValue + 0.9));
}

void rotateMotor(int power) {
  if (setPinOutput(SERVOMOTOR) && power >= 0 && power <= 255) {
    analogWrite(SERVOMOTOR, power);
  }
}

int getMaxLDRValue() {
  return 1023;
}
int getLDRValue(int maxValue) {
  defaultInitToOutput();
  return static_cast<int>(static_cast<float>(analogRead(LDR)) /
    getMaxPotentiometerValue() * (maxValue + 0.9));
}
