#ifndef Pnc_h
#define Pnc_h
#include "Arduino.h"

bool setPinInput(int pin);
bool setPinOutput(int pin);

void setLedOn(int ledNumber);
void setLedOff(int ledNumber);

void setAllLedOn();
void setAllLedOff();

void toggleLed(int ledNumber);
int getDistanceInCm();

void print(char* text);
void print(int value);
void print(boolean value);
void print(String value);

void println(char* text);
void println(int value);
void println(boolean value);
void println(String value);

bool isDataAvailable();
int readData();
char readDataChar();
String readDataLine();

boolean isButtonPressed();
boolean haveClicked();

void playSound(int frequency);
void playSound(int frequency, int duration);
void stopSound();
void successMusic();
void failMusic();


int getPotentiometerValue(int maxValue);

void rotateMotor(int power);

int getLDRValue(int maxValue);

#endif
