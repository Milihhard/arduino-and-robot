/* This program aims at implementing a ping pong game 
The movement of the ball is "represented" by the movement of light along the LEDs
When the last LED is turned on, the player has a short time lapse to press the button
If the button is pressed during this short time lapse, the ball goes back
Game ends otherwise and the player is notified of his failure by a sound
*/ 

// We initialize the variables (pin numbers to which LED are connected)
int pinLEDs[]={8, 9, 10, 11, 12, 13};
int NLEDs=sizeof(pinLEDs)/sizeof(int)-1; // Size of the array
int pinButton=2;
int pinBuzzer=6;
int actualTurn = 0;
int totalTurn = 2;
int timeLaps=1000;
void setup() {
  // put your setup code here, to run once:

  // Initialize communication with computer
  Serial.begin(9600);
  // We initialize modes
  for (int i=0; i <= NLEDs; i++) {
    pinMode(pinLEDs[i], OUTPUT);
  }
  pinMode(pinButton, INPUT);
  pinMode(pinBuzzer, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int success = false;
  if(actualTurn % 2 == 0) {
    leftToRight(timeLaps/pow(1.5,floor(actualTurn / 2)));
  } else {
    rightToLeft(timeLaps/pow(1.5,floor(actualTurn / 2)));
  }
  success = checkSuccess(timeLaps/pow(1.5,floor(actualTurn / 2)));
  if (success == false) {
    Serial.println("Failure :(");
    toneFail();
    restartGame();
  } else {
    Serial.println("Success!");
    if(actualTurn == totalTurn - 1){
      toneSuccess();
      restartGame();
    }
  }
  actualTurn++;
}

void leftToRight(int timeLaps) {
  digitalWrite(pinLEDs[0],HIGH);
  delay(timeLaps);
  for (int k=1; k <= NLEDs; k++) {
    digitalWrite(pinLEDs[k-1], LOW);
    digitalWrite(pinLEDs[k], HIGH);
    delay(timeLaps);
  }
  digitalWrite(pinLEDs[NLEDs],LOW);
}

void rightToLeft(int timeLaps) {
  digitalWrite(pinLEDs[NLEDs],HIGH);
  delay(timeLaps);
  for (int k=1; k <= NLEDs; k++) {
    digitalWrite(pinLEDs[NLEDs-k+1], LOW);
    digitalWrite(pinLEDs[NLEDs-k], HIGH);
    delay(timeLaps);
  }
  digitalWrite(pinLEDs[0],LOW);
}

bool checkSuccess(int timeLaps) {
  long startTime=millis();
  boolean isPressed=false;
  //if have clicked before
  if(checkIfPressed() == true){
    return false;
  }
  while (millis() < startTime+timeLaps && isPressed==false) {
    isPressed = checkIfPressed();
  }
  return isPressed;
  
}

bool checkIfPressed() {
  // variables to hold the new and old switch states
  boolean oldSwitchState = LOW;
  boolean newSwitchState1 = LOW;
  boolean newSwitchState2 = LOW;
  boolean newSwitchState3 = LOW;
  
  newSwitchState1 = digitalRead(pinButton);
  delay(1);
  newSwitchState2 = digitalRead(pinButton);
  delay(1);
  newSwitchState3 = digitalRead(pinButton);
 
  // if all 3 values are the same we can continue
  if ( (newSwitchState1==newSwitchState2) && (newSwitchState1==newSwitchState3))
  {
      if (newSwitchState1 != oldSwitchState) 
      {
         // has the button switch been closed?
         if ( newSwitchState1 == HIGH )
         {
             return true;
         }
         oldSwitchState = newSwitchState1;
      } 
  }
  return false; 
}

void toneSuccess() {
  Serial.println("You win the game!");
  for(int i = 0; i < 2; i++) {
    digitalWrite(pinLEDs[0],HIGH);
    tone(pinBuzzer,880);
    delay(100);
    digitalWrite(pinLEDs[0],LOW);
    digitalWrite(pinLEDs[1],HIGH);
    tone(pinBuzzer,988);
    delay(100);
    digitalWrite(pinLEDs[1],LOW);
    digitalWrite(pinLEDs[2],HIGH);
    tone(pinBuzzer,523);
    delay(100);
    digitalWrite(pinLEDs[2],LOW);
    digitalWrite(pinLEDs[3],HIGH);
    tone(pinBuzzer,988);
    delay(100);
    digitalWrite(pinLEDs[3],LOW);
    digitalWrite(pinLEDs[4],HIGH);
    tone(pinBuzzer,523);
    delay(100);
    digitalWrite(pinLEDs[4],LOW);
    digitalWrite(pinLEDs[5],HIGH);
    tone(pinBuzzer,587);
    delay(100);
    digitalWrite(pinLEDs[5],LOW);
    digitalWrite(pinLEDs[4],HIGH);
    tone(pinBuzzer,523);
    delay(100);
    digitalWrite(pinLEDs[4],LOW);
    digitalWrite(pinLEDs[3],HIGH);
    tone(pinBuzzer,587);
    delay(100);
    digitalWrite(pinLEDs[3],LOW);
    digitalWrite(pinLEDs[2],HIGH);
    tone(pinBuzzer,659);
    delay(100);
    digitalWrite(pinLEDs[2],LOW);
    digitalWrite(pinLEDs[1],HIGH);
    tone(pinBuzzer,587);
    delay(100);
    digitalWrite(pinLEDs[1],LOW);
    digitalWrite(pinLEDs[0],HIGH);
    tone(pinBuzzer,659);
    delay(100);
    digitalWrite(pinLEDs[0],LOW);
    tone(pinBuzzer,659);
    delay(100);
  }
  noTone(pinBuzzer);
}
void toneFail(){
  tone(pinBuzzer,392);
  delay(250);
  tone(pinBuzzer,262);
  delay(500);
  noTone(pinBuzzer);
  //delay(4000);
}

void restartGame(){
  actualTurn = -1;
  Serial.println("Press the button to start again!");
  while(checkIfPressed() == false) {
    // do nothing
  }
}

