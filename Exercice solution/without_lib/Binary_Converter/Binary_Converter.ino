/* This program converts a decimal number entered in the Serial monitor in a binary
 * Then it displays the binary with the LEDs
 * !/_\! This program requires to change the setting "No line ending" to "New line" in the Serial monitor
 */

// We initialize the variables (pin numbers to which LED are connected)
int pinLEDs[]={9,10,11,12,13};

// For incoming serial data
int incomingByte = 0;   
// String to hold input
String inString = "";
// Check if non digit character entered
int nonDigit = 0;


void setup() {
  // We initialize modes (input/output)
  for (int i=0; i <= sizeof(pinLEDs)/sizeof(int)-1; i++) {
    pinMode(pinLEDs[i], OUTPUT);
  }
  // Initialize the communication with computer
  Serial.begin(9600);

  // send an intro:
  Serial.println("Binary converter:");
}

void loop() {
  char inByte;
  // Read serial input:
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    if (isDigit(inChar)) {
      // convert the incoming byte to a char and add it to the string:
      inString += (char)inChar;
    }
    else {
      nonDigit += 1;
    }
    // if you get a newline, print the string, then the string's value:
    if (inChar == '\n') {
      // nonDigit at least equals 1 with the '\n' character; if more than 1, it means the user entered non digit character in the serial
      if(nonDigit > 1) {
        Serial.println("Error: you did not enter a number.");
      }
      else {
        int inputNumber = inString.toInt();
        if(inputNumber >= 0 && inputNumber <= 31){
          // Convert to binary and display the result on the LEDs
          convertToBinary(inputNumber); 
        }
        else{
          Serial.println("Error: the number entered is out of range. Please enter a number between 0 and 31."); 
        }        
      }
      // clear the string for new input:
      inString = "";
      nonDigit = 0;
    }
  }
}

// Convert int to binary and display the result on the LEDs
void convertToBinary(int inputNumber) {
  String inputBinary="";
  int tmpNum = inputNumber;
  int i = 0;
  while(tmpNum / 2 != 0) {
    // Fill inputBinary with '0' or '1'
    inputBinary += (char)tmpNum % 2;
    tmpNum = tmpNum / 2;
    i++;
  }
  inputBinary += (char)tmpNum % 2;
  Serial.println(inputBinary);

  // Display the LEDs associated with '1'
  for(int j = 0; j <= i; j ++){
    if(inputBinary[j] == '1'){
      digitalWrite(pinLEDs[j],HIGH);
    } else {
      digitalWrite(pinLEDs[j],LOW);
    }
  }
  // Turn off the other LEDs
  for(int j = i+1; j < 5; j++){
    digitalWrite(pinLEDs[j],LOW);
  }
}

