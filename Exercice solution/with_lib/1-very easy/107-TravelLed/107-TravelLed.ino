int startLed = 2;
int endLed = 13;
int timeToWait = 1000;
void setup() {
}

void loop() {
  for(int i = startLed; i <= endLed; i++) {
    setLedOn(i);
    delay(timeToWait);
    setLedOff(i);
  }
}
