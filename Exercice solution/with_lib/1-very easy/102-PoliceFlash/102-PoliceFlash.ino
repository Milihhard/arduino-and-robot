int FIRST_LED = 2;
int SECOND_LED = 3;
void setup() {
}

void loop() {
  setLedOn(FIRST_LED);
  delay(250);
  setLedOff(FIRST_LED);
  setLedOn(SECOND_LED);
  delay(250);
  setLedOff(SECOND_LED);
}
