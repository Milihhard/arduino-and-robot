int GREEN_LIGHT = 2;
int ORANGE_LIGHT = 3;
int RED_LIGHT = 4;
int DELAY_GREEN = 10000;
int DELAY_ORANGE = 2000;
int DELAY_RED = 8000;
void setup() {
}

void loop() {
  //greenLight
  setLedOff(RED_LIGHT);
  setLedOn(GREEN_LIGHT);
  delay(DELAY_GREEN);
  //orangLight
  setLedOff(GREEN_LIGHT);
  setLedOn(ORANGE_LIGHT);
  delay(DELAY_ORANGE);
  //redLight
  setLedOff(ORANGE_LIGHT);
  setLedOn(RED_LIGHT);
  delay(DELAY_RED);
}
