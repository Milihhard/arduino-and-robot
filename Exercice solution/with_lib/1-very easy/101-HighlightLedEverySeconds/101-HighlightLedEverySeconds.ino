// Led which will blink
int led = 2;

void setup() {
  // No setup
  // The library does it for us
}

void loop() {
  // Using library to set led on
  setLedOn(led);
  delay(1000);
  // Using library to set led off
  setLedOff(led);
  delay(1000);
}
