unsigned long timeToWait = 1000;
unsigned long timeToRecord= millis();
int led = 3;
void setup() {
}

void loop() {

  if(haveClicked()){
    timeToWait = timeToWait / 2;
  }
  if(millis() - timeToRecord > timeToWait) {
    toggleLed(led);
    timeToRecord = millis();
  }
}
