/* This program aims at implementing a ping pong game
The movement of the ball is "represented" by the movement of light along the LEDs
When the last LED is turned on, the player has a short time lapse to press the button
If the button is pressed during this short time lapse, the ball goes back
Game ends otherwise and the player is notified of his failure by a sound
*/

// We initialize the variables (pin numbers to which LED are connected)
int actualTurn = 0;
int totalTurn = 10;
int timeLaps=1000;
int startLed = 4;
int endLed = 13;
void setup() {
}

void loop() {
  // put your main code here, to run repeatedly:
  int success = false;
  if(actualTurn % 2 == 0) {
    leftToRight(timeLaps/pow(1.5,floor(actualTurn / 2)));
  } else {
    rightToLeft(timeLaps/pow(1.5,floor(actualTurn / 2)));
  }
  success = checkSuccess(timeLaps/pow(1.5,floor(actualTurn / 2)));
  if (success == false) {
    println("Failure :(");
    failMusic();
    restartGame();
  } else {
    println("Success!");
    if(actualTurn == totalTurn - 1){
      println("You win the game!");
      successMusic();
      restartGame();
    }
  }
  actualTurn++;
}

void leftToRight(int timeLaps) {
  setLedOn(startLed);
  delay(timeLaps);
  for (int k=startLed + 1; k <= endLed; k++) {
    setLedOff(k - 1);
    setLedOn(k);
    delay(timeLaps);
  }
  setLedOff(endLed);
}

void rightToLeft(int timeLaps) {
  setLedOn(endLed);
  delay(timeLaps);
  for (int k=endLed; k > startLed; k--) {
    setLedOff(k);
    setLedOn(k - 1);
    delay(timeLaps);
  }
  setLedOff(startLed);
}

bool checkSuccess(int timeLaps) {
  long startTime=millis();
  boolean isPressed=false;
  //if have clicked before
  if(haveClicked() == true){
    return false;
  }
  while (millis() < startTime+timeLaps && isPressed==false) {
    isPressed = haveClicked();
  }
  return isPressed;

}

void restartGame(){
  actualTurn = -1;
  Serial.println("Press the button to start again!");
  while(!haveClicked());
}
