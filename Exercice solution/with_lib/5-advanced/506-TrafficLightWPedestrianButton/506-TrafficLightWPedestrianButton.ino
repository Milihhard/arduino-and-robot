int GREEN_LIGHT = 2;
int ORANGE_LIGHT = 3;
int RED_LIGHT = 4;
int PIEDESTRIAN_GREEN = 5;
int PIEDESTRIAN_RED = 6;
int DELAY_GREEN = 10000;
int DELAY_ORANGE = 2000;
int DELAY_RED = 8000;
unsigned long timeToWait= DELAY_GREEN;
unsigned long timeToRecord= millis();
bool buttonPressed= false;
int step = 1;
void setup() {
  setLedOn(GREEN_LIGHT);
  setLedOn(PIEDESTRIAN_RED);
}

void loop() {

  if(haveClicked() && step == 1 && millis() - timeToRecord > 1000){
    timeToWait = 1000;
    reinitializeTimeRecord();
  }
  if(millis() - timeToRecord > timeToWait) {
    if(step == 0) {
      //greenLight
      setLedOff(RED_LIGHT);
      setLedOn(GREEN_LIGHT);
      setLedOff(PIEDESTRIAN_GREEN);
      setLedOn(PIEDESTRIAN_RED);
      reinitializeTimeRecord();
      timeToWait = DELAY_GREEN;
      step = 1;
    } else if (step == 1){
      //orangLight
      setLedOff(GREEN_LIGHT);
      setLedOn(ORANGE_LIGHT);
      reinitializeTimeRecord();
      timeToWait = DELAY_ORANGE;
      step = 2;
    } else if (step == 2){
      //redLight
      setLedOff(ORANGE_LIGHT);
      setLedOn(RED_LIGHT);
      setLedOff(PIEDESTRIAN_RED);
      setLedOn(PIEDESTRIAN_GREEN);
      reinitializeTimeRecord();
      timeToWait = DELAY_RED;
      step = 0;
    }
  }
}

void reinitializeTimeRecord(){
  timeToRecord = millis();
}
