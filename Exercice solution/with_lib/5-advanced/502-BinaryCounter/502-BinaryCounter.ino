int startLed = 3;

int num = 0;
String str = "";
void setup() {
}

void loop() {
  if(haveClicked()){
    int i = 0;
    str = "";
    if(num > 2047) {
      num = 0;
      setAllLedOff();
    }
    println(num);
    int tmpNum = num;
    while(tmpNum / 2 != 0){
      str += (char)(tmpNum % 2 + '0');
      tmpNum = tmpNum / 2;
      i++;
    }
    str += (char)(tmpNum % 2 + '0');
    println(str);
    for(int j = 0; j <= i; j ++){
      if(str[j] == '1'){
        setLedOn(startLed + j);
      } else {
        setLedOff(startLed + j);
      }
    }
    println("");
    num++;
  }
}
