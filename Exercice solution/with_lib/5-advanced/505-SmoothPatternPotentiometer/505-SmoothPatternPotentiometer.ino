int actualLed = 1;
int sens = 1;
unsigned long waitingTime = millis();
int startLed = 2;
int endLed = 13;
void setup() {
}

void loop() {

  // delay(10);
  int value = getPotentiometerValue(1000);
  if(millis() - waitingTime > value) {
    setLedOff(actualLed);
    actualLed += sens;
    if(actualLed == endLed){
      sens = -1;
    } else if (actualLed == startLed){
      sens = 1;
    }
    setLedOn(actualLed);
    println(value);
    waitingTime = millis();
  }
}
