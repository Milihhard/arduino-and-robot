// String to hold input
String inString = "";
// Check if non digit character entered
int nonDigit = false;
int startLed = 2;

void setup() {

  // send an intro:
  println("Binary converter:");
}

void loop() {
  if (isDataAvailable()) {
    inString = readDataLine();
    // Test if the string is a number
    for(int i = 0; i < inString.length(); i++){
      if (!isDigit(inString[i])) {
        nonDigit = true;
      }
    }
    // nonDigit at least equals 1 with the '\n' character; if more than 1, it means the user entered non digit character in the serial
    if(nonDigit) {
      println("Error: you did not enter a number.");
    } else {
      int inputNumber = inString.toInt();
      if(inputNumber >= 0 && inputNumber <= 4095){
        // Convert to binary and display the result on the LEDs
        convertToBinary(inputNumber);
      } else {
        println("Error: the number entered is out of range. Please enter a number between 0 and 31.");
      }
    }
    inString = "";
    nonDigit = false;
  }
}

// Convert int to binary and display the result on the LEDs
void convertToBinary(int inputNumber) {
  int inputBinary[5];
  int tmpNum = inputNumber;
  int i = 0;
  while(tmpNum / 2 != 0) {
    // Fill inputBinary with '0' or '1'
    inputBinary[i] = tmpNum % 2;
    tmpNum = tmpNum / 2;
    i++;
  }
  inputBinary[i] = tmpNum % 2;

  // Display the LEDs associated with '1'
  for(int j = 0; j <= i; j ++){
    if(inputBinary[j] == 1){
      setLedOn(j + startLed);
    } else {
      setLedOff(j + startLed);
    }
  }
  // Turn off the other LEDs
  for(int j = i+1; j < 5; j++){
    setLedOff(j + startLed);
  }
}
