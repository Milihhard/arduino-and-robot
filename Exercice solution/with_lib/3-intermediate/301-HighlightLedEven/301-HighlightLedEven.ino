int startLed = 2;
int endLed = 13;
int timeToWait = 1000;
void setup() {
}

void loop() {
  turnOnEvenLed();
  delay(timeToWait);
  turnOffEvenLed();
  delay(timeToWait);
}

void turnOnEvenLed(){
  for(int i = startLed; i <= endLed; i ++) {
    if(i % 2 == 0) {
      setLedOn(i);
    }
  }
}

void turnOffEvenLed(){
  for(int i = startLed; i <= endLed; i ++) {
    if(i % 2 == 0) {
      setLedOff(i);
    }
  }
}
