int startLed = 2;
int endLed = 13;
int timeToWait = 50;
int maxValue = 12;

void setup() {
}

void loop() {

  int value = getLDRValue(maxValue);
  if(value > 8) {
    for(int i = startLed; i <= endLed; i++){
      setLedOn(i);
      delay(timeToWait);
    }
    for(int i = startLed; i <= endLed; i++){
      setLedOff(i);
      delay(timeToWait);
    }
  } else {
    setAllLedOff();
  }
  delay(10);
}
