String result = "";

void setup() {
}

void loop() {

  if(isDataAvailable()) {
    String line = readDataLine();
    print("Input: ");
    println(line);
    print("Output: ");
    for(int i = 0; i < line.length(); i++){
      result += line[line.length - i - 1];
    }
    println(result);
  }
}
