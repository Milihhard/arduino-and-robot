// String to hold input
String inString = "";
int startLed = 2;

void setup() {

  // send an intro:
  println("Binary display:");
}

void loop() {
  if (isDataAvailable()) {
    inString = readDataLine();
    setAllLedOff();
    for(int i = 0; i < inString.length(); i++){
      if(inString[inString.length() - i - 1] == '1'){
        setLedOn(startLed + i);
      } else {
        setLedOff(startLed + i);
      }
    }
  }
}
