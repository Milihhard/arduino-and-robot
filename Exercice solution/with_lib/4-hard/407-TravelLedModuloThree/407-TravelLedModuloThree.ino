int startLed = 2;
int endLed = 13;
int timeToWait = 1000;

void setup() {
}

void loop() {

  for(int modulo = 0; modulo < 3; modulo ++){
    setAllLedOff();
    for(int i = startLed; i <= endLed; i ++) {
      if(i % 3 == modulo) {
        setLedOn(i);
      } else {
        setLedOff(i);
      }
    }
    delay(timeToWait);

  }
}
