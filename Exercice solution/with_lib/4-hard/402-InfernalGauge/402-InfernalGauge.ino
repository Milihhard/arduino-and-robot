int startLed = 3;
int endLed = 13;
int timeToWait = 1000;
void setup() {
}

void loop() {
  if(isButtonPressed()) {
    for(int i = startLed; i <= endLed; i++){
      setLedOn(i);
      delay(timeToWait);
    }
    for(int i = endLed; i >= startLed; i--){
      setLedOff(i);
      delay(timeToWait);
    }
    timeToWait = timeToWait / 2;
  } else {
    timeToWait = 1000;
    setAllLedOff();
  }
}
