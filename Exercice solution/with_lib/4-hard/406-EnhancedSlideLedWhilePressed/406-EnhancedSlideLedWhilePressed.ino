int startLed = 3;
int endLed = 13;
int timeToWait = 250;
int led = startLed;
void setup() {
}

void loop() {
  if(isButtonPressed()) {
    toggleLed(led);
    led++;
    delay(timeToWait);
    if(led > endLed) {
      led = startLed;
    }
  }
}
