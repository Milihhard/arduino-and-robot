int currentLed = 3;
int endLed = 13;
void setup() {
}

void loop() {
  if(haveClicked()){
    if(currentLed <= endLed){
      setLedOn(currentLed);
      if(currentLed == endLed){
        playSound(440, 500);
      }
      currentLed++;
    }
  }
}
