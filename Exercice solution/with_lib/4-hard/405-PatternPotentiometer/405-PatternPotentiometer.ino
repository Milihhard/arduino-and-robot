int startLed = 2;
int endLed = 13;
void setup() {
}

void loop() {

  int value = getPotentiometerValue(1000);
  for(int i = startLed; i < endLed; i++){
    setLedOff(i - 1);
    setLedOn(i);
    delay(value);
  }
  setLedOff(endLed - 1);
  for(int i = endLed; i > startLed; i--){
    setLedOff(i + 1);
    setLedOn(i);
    delay(value);
  }
  setLedOff(startLed + 1);
}
