bool isEven = true;
int startLed = 3;
int endLed = 13;
void setup() {
}

void loop() {
  if(haveClicked()){
    if(isEven){
      turnOnEvenLed();
      isEven = false;
    } else {
      turnOnOddLed();
      isEven = true;
    }
  }
}

void turnOnEvenLed(){
  for(int i = startLed; i <= endLed; i ++) {
    if(i % 2 == 0) {
      setLedOn(i);
    } else {
      setLedOff(i);
    }
  }
}

void turnOnOddLed(){
  for(int i = startLed; i <= endLed; i ++) {
    if(i % 2 == 1) {
      setLedOn(i);
    } else {
      setLedOff(i);
    }
  }
}
