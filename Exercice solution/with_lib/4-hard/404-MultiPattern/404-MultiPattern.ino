int timeToWait = 50;
int startLed = 2;
int endLed = 13;
char pattern = 's';
void setup() {
}

void loop() {
  if(isDataAvailable()){
    char readSerial = readDataChar();
    if(readSerial == 's' || readSerial == 'g' || readSerial == 'r'){
      pattern = readSerial;
    }
  }
  switch(pattern){
    case 's':
      slideLedPattern();
      break;
    case 'g':
      gaugePattern();
      break;
    case 'r':
      returnTravelLedPattern();
      break;
  }
}


void slideLedPattern(){
  for(int i = startLed; i <= endLed; i++){
    setLedOn(i);
    delay(timeToWait);
  }
  for(int i = startLed; i <= endLed; i++){
    setLedOff(i);
    delay(timeToWait);
  }
}

void gaugePattern(){
  for(int i = startLed; i <= endLed; i++){
    setLedOn(i);
    delay(timeToWait);
  }
  for(int i = endLed; i >= startLed; i--){
    setLedOff(i);
    delay(timeToWait);
  }
}

void returnTravelLedPattern(){
  for(int i = startLed; i < endLed; i++){
    setLedOff(i - 1);
    setLedOn(i);
    delay(timeToWait);
  }
  setLedOff(endLed - 1);
  for(int i = endLed; i > startLed; i--){
    setLedOff(i + 1);
    setLedOn(i);
    delay(timeToWait);
  }
  setLedOff(startLed + 1);
}
