int startLed = 2;
int maxValue = 12;

void setup() {
}

void loop() {

  int nbLedOn = getLDRValue(maxValue);
  setAllLedOff();
  for(int i = 0; i < nbLedOn; i++) {
    setLedOn(startLed + i);
  }
  delay(10);
}
