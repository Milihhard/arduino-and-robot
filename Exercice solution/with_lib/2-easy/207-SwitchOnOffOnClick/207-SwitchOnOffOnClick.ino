bool isOn = false;
int led = 6;
void setup() {
}

void loop() {
  if(haveClicked()){
    if(isOn){
      setLedOff(led);
    } else {
      setLedOn(led);
    }
    isOn = !isOn;
  }
}
