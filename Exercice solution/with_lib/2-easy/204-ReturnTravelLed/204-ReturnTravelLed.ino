int startLed = 2;
int endLed = 13;
int timeToWait = 500;
void setup() {
}

void loop() {

  for(int i = startLed; i < endLed; i++){
    setLedOn(i);
    delay(timeToWait);
    setLedOff(i);
  }
  for(int i = endLed; i > startLed; i--){
    setLedOn(i);
    delay(timeToWait);
    setLedOff(i);
  }
}
