int startLed = 2;
int endLed = 6;
void setup() {
}

void loop() {


  int value = getDistanceInCm();
  for(int i = startLed; i <= endLed; i++) {
    setLedOff(i);
  }
  if(value <= 100) {
    setLedOn(startLed);
  }
  if(value <= 80) {
    setLedOn(startLed + 1);
  }
  if(value <= 50) {
    setLedOn(startLed + 2);
  }
  if(value <= 30) {
    setLedOn(startLed + 3);
  }
  if(value <= 10) {
    setLedOn(endLed);
    playSound(440);
  } else {
    stopSound();
  }
  delay(10);
}
