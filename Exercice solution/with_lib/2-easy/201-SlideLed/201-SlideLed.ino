int startLed = 2;
int endLed = 13;
int timeToWait = 250;
void setup() {
}

void loop() {
  for(int i = startLed; i <= endLed; i++){
    setLedOn(i);
    delay(timeToWait);
  }
  for(int i = startLed; i <= endLed; i++){
    setLedOff(i);
    delay(timeToWait);
  }
}
