int startLed = 2;
int endLed = 13;
int timeToWait = 1000;
void setup() {
}

void loop() {
  for(int i = startLed; i <= endLed; i++) {
    if(i % 2 == 0){
      setLedOn(i);
      delay(timeToWait);
      setLedOff(i);
    }
  }
}
