int GREEN_LIGHT = 2;
int ORANGE_LIGHT = 3;
int RED_LIGHT = 4;
int PIEDESTRIAN_GREEN = 5;
int PIEDESTRIAN_RED = 6;
int DELAY_GREEN = 10000;
int DELAY_ORANGE = 2000;
int DELAY_RED = 8000;
void setup() {
}

void loop() {
  //greenLight
  setLedOff(RED_LIGHT);
  setLedOn(GREEN_LIGHT);
  setLedOff(PIEDESTRIAN_GREEN);
  setLedOn(PIEDESTRIAN_RED);
  delay(DELAY_GREEN);
  //orangLight
  setLedOff(GREEN_LIGHT);
  setLedOn(ORANGE_LIGHT);
  delay(DELAY_ORANGE);
  //redLight
  setLedOff(ORANGE_LIGHT);
  setLedOn(RED_LIGHT);
  setLedOff(PIEDESTRIAN_RED);
  setLedOn(PIEDESTRIAN_GREEN);
  delay(DELAY_RED);
}
