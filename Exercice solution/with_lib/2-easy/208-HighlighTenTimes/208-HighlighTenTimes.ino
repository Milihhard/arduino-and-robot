int currentStep = 0;
int led = 2;
int timeToWait = 1000;
void setup() {
}

void loop() {
  if(currentStep < 10){
    setLedOn(led);
    delay(timeToWait);
    setLedOff(led);
    delay(timeToWait);
    currentStep++;
  }

}
