int startLed = 2;
int nbLed = 5;
void setup() {
}

void loop() {

  int value = getDistanceInCm();
  setAllLedOff();
  if(value <= nbLed * 10){
    for(int i = 0; i < nbLed - value / 10; i++) {
      setLedOn(startLed + i);
    }
  }
  if(value <= 10) {
    playSound(440);
  } else {
    stopSound();
  }
  delay(10);
}
